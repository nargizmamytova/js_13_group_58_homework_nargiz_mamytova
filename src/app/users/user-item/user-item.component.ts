import { Component, Input } from '@angular/core';
import { User } from '../../shared/users.module';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent {
  @Input() newUser!: User;

  constructor(private groupService: GroupService) {
  }
  getRole() {
    if (this.newUser.role === 'value1') {
      this.newUser.role = 'user'
    } else if (this.newUser.role === 'value2') {
      this.newUser.role = 'editor'
    } else if (this.newUser.role === 'value3') {
      this.newUser.role = 'admin'
    }
    return this.newUser.role
  }
  onClick(){
    this.groupService.addToSelectedGroup(this.newUser)
  }
}
