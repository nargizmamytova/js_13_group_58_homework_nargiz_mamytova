import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/userService';
import { User } from '../shared/users.module';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{
  users: User[] = [];
  constructor(private userService: UserService, private groupService: GroupService) {
  }
  ngOnInit() {
    this.users = this.userService.getUsers();
    this.userService.usersChange.subscribe((users: User[])=> {
      this.users = users;
    })
  }

}
