import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UsersComponent } from './users/users.component';
import { UserItemComponent } from './users/user-item/user-item.component';
import { FormsModule } from '@angular/forms';
import { UserService } from './shared/userService';
import { GroupsComponent } from './groups/groups.component';
import { GroupService } from './shared/group.service';
import { GroupItemComponent } from './groups/group-item/group-item.component';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    UsersComponent,
    UserItemComponent,
    GroupsComponent,
    GroupItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UserService, GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
