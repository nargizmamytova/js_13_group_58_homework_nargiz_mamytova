import { Group } from './group.model';
import { EventEmitter } from '@angular/core';
import { User } from './users.module';

export class GroupService{
  groupItemsChange = new EventEmitter<Group[]>();
  selectedGroup!: Group;
  selectedGroupChange = new EventEmitter<Group>();
  private arrayGroup: Group[] = [
    new Group('Tennis club'),
    new Group('Voleiball club')
  ];

  getGroup(){
    return this.arrayGroup.slice();
  }
  addToArrayGroup(group: Group){
    this.arrayGroup.push(group);
    this.groupItemsChange.emit(this.arrayGroup);
  }
  selectGroup(group: Group){
    this.selectedGroup = group;
    this.selectedGroupChange.emit(group)
  }

  addToSelectedGroup(newUser: User) {
    this.selectedGroup.addUser(newUser)
  }
}
