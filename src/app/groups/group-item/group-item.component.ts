import { Component, Input, OnInit } from '@angular/core';
import { Group } from '../../shared/group.model';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-group-item',
  templateUrl: './group-item.component.html',
  styleUrls: ['./group-item.component.css']
})
export class GroupItemComponent implements OnInit{
  @Input() group!: Group;
  isSelected = false;

  constructor(private groupService: GroupService) {
  }
  ngOnInit() {
    if(this.groupService.selectedGroup === this.group){
      this.isSelected = true;
    }
    this.groupService.selectedGroupChange.subscribe((group: Group) => {
      if(group === this.group){
        this.isSelected = true;
      }else {
        this.isSelected = false;
      }
    })
  }
  onClick(){
      this.groupService.selectGroup(this.group);
  }

}
