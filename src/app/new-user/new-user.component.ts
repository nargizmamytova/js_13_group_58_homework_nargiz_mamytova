import { Component, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../shared/userService';
import { User } from '../shared/users.module';
import { Group } from '../shared/group.model';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],

})
export class NewUserComponent{

  @ViewChild('inputName')inputName!: ElementRef;
  @ViewChild('inputEmail')inputEmail!: ElementRef;
  @ViewChild('inputCheckbox')inputCheckbox!: ElementRef;
  @ViewChild('inputSelect')inputSelect!: ElementRef;
  @ViewChild('groupName')groupName!: ElementRef;

  constructor(private userService: UserService, private groupService: GroupService) {}
createNewUser(){
  const name = this.inputName.nativeElement.value;
  const email = this.inputEmail.nativeElement.value;
  const checkbox = this.inputCheckbox.nativeElement.value;
  const role = this.inputSelect.nativeElement.value;

  const newUser = new User(name, email, checkbox, role);
  this.userService.addUser(newUser);
}

  onDeleteClick() {
    this.inputName.nativeElement.value = '';
    this.inputEmail.nativeElement.value = '';
    this.inputCheckbox.nativeElement.value = '';
    this.inputSelect.nativeElement.value = '';
  }
  onChange() {
   this.inputCheckbox.nativeElement.value = 'on';

  }

  onCreate() {
    const user: User[] = [];
    const groupName = this.groupName.nativeElement.value;
    const group = new Group(groupName);
    this.groupService.addToArrayGroup(group);
  }

  onDelClick() {
    this.groupName.nativeElement.value = '';
  }
}
